# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

from .cli import main

if __name__ == "__main__":
    main()
